import { query } from '@angular/animations';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { HomePageComponent } from './home-page.component';
import { PostsAndUsersService } from '../posts-and-users.service';
import { from } from 'rxjs';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let sortAsc;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    sortAsc = component.sortAsc;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return sort ascendingly', () => {
    const result = sortAsc({date: 12}, {date: 3})
    expect(result).toBe(1);
  })
  it('should check for admin', () => {
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    expect($PostsAndUsersService.admin).toBeTruthy();
  })
  it('title should be Home page', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Home page');
  })
  it('should fetch posts from the service',async()=>{
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPosts').and.returnValue(from([{}, {}]))
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.posts).not.toBe([])
    })
  })
  it('should return unposted posts', fakeAsync(()=>{
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPostsOnlySend').and.returnValue(from([{}, {}]))
    tick()
    fixture.detectChanges();
    expect(component.posts).not.toBe([])
  }))
  it('should return posted only posts', fakeAsync(()=>{
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPostsNoSend').and.returnValue(from([{}, {}]))
    tick()
    fixture.detectChanges();
    expect(component.posts).not.toBe([])
  }))
  it('should return deleted posts', fakeAsync(()=>{
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'deletedArr').and.returnValue(from([{}, {}]))
    tick()
    fixture.detectChanges();
    expect(component.deletedArr).not.toBe([])
  }))


  //FORM FILTERING AND SORTING
  it('should filter out description or title a inputed date', fakeAsync(()=>{
    let arr = [{title: 'hey you'}, {title: 'I had a nice day'}];
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPosts').and.returnValue(from(arr))
    let compiled = fixture.debugElement.nativeElement.querySelector('[id="search"]')
    compiled.value = 'hey'
    tick()
    fixture.detectChanges();
    expect(component.posts).not.toBe(arr)
  }))
  it('should filter out a author', fakeAsync(()=>{
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPosts').and.returnValue(from([{author: 'Bohdan'}, {author: 'Valik'}]))
    let compiled = fixture.debugElement.nativeElement.querySelector('[id="auth"]')
    compiled.value = 'Bohdan'
    tick()
    fixture.detectChanges();
    expect(component.posts).toBe([{author: 'Bohdan'}])
  }))

  it('should filter out a type', fakeAsync(()=>{
    let arr = [{type: 'Post'}, {type: 'News'}];
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPosts').and.returnValue(from(arr))
    component.onType('News')
    tick()
    fixture.detectChanges();
    expect(component.posts).not.toBe(arr)
  }))
  it('should filter out a type from input field', fakeAsync(()=>{
    let arr = [{type: 'Post'}, {type: 'News'}];
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPosts').and.returnValue(from(arr))
    let compiled = fixture.debugElement.nativeElement.querySelector('.form-control mult')
    let selectedOption = compiled.innerHTML.querySelector('[selected]')
    component.onType(selectedOption)
    tick()
    fixture.detectChanges();
    expect(component.posts).not.toBe(arr)
  }))
  it('should toggle sorting on checkbox', fakeAsync(()=>{
    let arr = [{date: "12"}, {date: '34'}];
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    const spy = spyOn($PostsAndUsersService, 'getPosts').and.returnValue(from(arr))
    let compiled = fixture.debugElement.nativeElement.querySelector('[type="checkbox"]')
    compiled.click();
    tick()
    fixture.detectChanges();
    expect(component.posts).not.toBe(arr)
  }))
});
