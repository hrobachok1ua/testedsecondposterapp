import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { PostsAndUsersService } from '../../posts-and-users.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss']
})
export class PostPageComponent implements OnInit, OnDestroy {

  post = {};

  constructor(private http: PostsAndUsersService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.http.onDetail = true;
    let param = '';
    this.route.params.subscribe(params => {
      param = params.id

      this.http.getPosts().subscribe(posts => {
        const el = posts.filter(e => e.key === param);

        this.post = el[0];
      })
    });
  }
  ngOnDestroy() {
    this.http.onDetail = false;
  }
}
