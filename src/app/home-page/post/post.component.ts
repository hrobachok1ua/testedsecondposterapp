import { Component, OnInit, Input } from '@angular/core';
import { PostsAndUsersService } from '../../posts-and-users.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() author: string;
  @Input() type: string;
  @Input() title: string;
  @Input() description: string;
  @Input() date: Date;
  @Input() posted: string;
  @Input() keyCode: string;
  @Input() src: string;

  constructor(private http: PostsAndUsersService) { }

  admin = this.http.admin
  showDeletedBin = this.http.showDeletedBin
  onDetail = this.http.onDetail
  isBtnVisible = this.admin && !this.showDeletedBin && !this.onDetail;

  ngOnInit() { }
  delete(keyCode) {
    // return this.http.deletePost(keyCode);
    let posts$ = [];
    this.http.getPosts().subscribe(posts => {
      posts$ = posts;

      const c = keyCode.key;

      const el = posts$.filter(e => e.key === c);

      this.http.deletedArr.push(el[0]);
      this.http.shouldRender = true;
    });
  }
  photoErrorHandler(event) {
    event.target.src = "https://cdn.browshot.com/static/images/not-found.png";
  }
}
