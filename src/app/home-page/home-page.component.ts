import { Component, OnInit, DoCheck } from '@angular/core';
import { PostsAndUsersService } from '../posts-and-users.service';
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('postsStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(
              '50ms',
              animate(
                '550ms ease-out',
                style({ opacity: 1, transform: 'translateY(0px)' })
              )
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class HomePageComponent implements OnInit, DoCheck {

  p: number = 1;

  posts = [];
  // counter = 5;
  deletedArr = [];

  currentDate = '2019-02-10'
  showDeletedBinFlag = this.postsAndUsersService.showDeletedBin
  admin = this.postsAndUsersService.admin

  constructor(private postsAndUsersService: PostsAndUsersService) { }

  restrictShowDeleted(posts) {
    const d = this.postsAndUsersService.deletedArr;
    const p = posts;
    for (let dEl in d) {
      for (let el in p) {
        if (p[el].key === d[dEl].key) {
          p.splice(+el, 1);
        }
      }
    }
    return p;
  }

  showDeletedBin() {
    this.postsAndUsersService.showDeletedBin = !this.postsAndUsersService.showDeletedBin;
  }

  sortAsc(a, b) {
    if (a.date > b.date) {
      return 1;
    }
    if (a.date < b.date) {
      return -1;
    }
    return 0;
  }

  loadDifferently(callback) {
    if (this.postsAndUsersService.admin) {
      this.postsAndUsersService.getPosts()
        .subscribe(callback);
    } else {
      this.postsAndUsersService.getPostsOnlySend()
        .subscribe(callback);
    }
  }

  loadPosts() {
    this.loadDifferently(post => {
      this.posts = this.restrictShowDeleted(post.sort(this.sortAsc).filter(el => el.date > this.currentDate));
    })
  }

  ngDoCheck() {
    if (this.postsAndUsersService.shouldRender) {
      this.postsAndUsersService.shouldRender = false;
      this.loadPosts();
    }

    this.deletedArr = this.postsAndUsersService.deletedArr;
  }

  ngOnInit() {
    this.loadPosts();
  }

  methodAndType() {
    this.loadDifferently(post => {
      this.posts = this.restrictShowDeleted(post.sort(this.sortAsc).reverse().filter(el => el.date > this.currentDate));
    })
  }

  onMethod(el) {
    if (el.checked) {
      this.loadPosts();
    } else {
      this.methodAndType();
    }
  }

  onType(element) {
    this.loadDifferently(post => {
      this.posts = this.restrictShowDeleted(post.sort(this.sortAsc).filter(el => el.type === element.value));
    })
  }

  authorAndSearch(el, callback) {
    if (el.value === '') {
      this.loadPosts();
    } else {
      this.loadDifferently(post => {
        const p = post.sort(this.sortAsc);
        this.posts = this.restrictShowDeleted(p.filter(callback)
          .filter(el => el.date > this.currentDate));
      })
    }
  }

  onAuthor(el) {
    this.authorAndSearch(el, e => e.author.includes(el.value));
  }

  onSearch(el) {
    this.authorAndSearch(el, e => (e.title.includes(el.value) || e.description.includes(el.value)));
  }

}
