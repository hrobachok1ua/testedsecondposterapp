import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { PostsAndUsersService } from '../posts-and-users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  name = '';
  email = '';
  verif = false;
  id = '';

  constructor(private http: PostsAndUsersService, private r: Router) { }

  ngOnInit() {
    const user = firebase.auth().currentUser;
    if (user != null) {
      this.name = user.displayName;
      this.email = user.email;
      this.verif = user.emailVerified;
      this.id = user.uid;
    }
  }

  changePassword(v, p) {
    firebase.auth()
      .signInWithEmailAndPassword(this.email, p.value)
      .then((userCredential) => {
        userCredential.user.updatePassword(v.value).then(() => {
          this.http.changed = true;
        }).catch((error) => {
          // An error happened.
        });
      });
  }

  changeEmail(v, p) {
    firebase.auth()
      .signInWithEmailAndPassword(this.email, p.value)
      .then((userCredential) => {
        userCredential.user.updateEmail(v.value).then(() => {
          this.http.changed = true;
        }).catch((error) => {
          // An error happened.
        });
      });
  }

  deleteAccount() {
    firebase.auth().currentUser.delete().then(() => {
      this.http.deletedAcc = true;
      this.r.navigate(['/']);
    }).catch((error) => {
      // An error happened.
    });
  }

}
