import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostsAndUsersService {

  postRef: AngularFireList<any>;
  posts: Observable<any[]>;

  userEmail = '';
  admin = false;
  checkEmail = false;
  deletedAcc = false;
  alreadyReg = false;
  changed = false;
  deletedArr = [];
  shouldRender = false;

  showDeletedBin = false;
  onDetail = false;

  constructor(private http: AngularFireDatabase, private r: Router) { }

  signInUser(email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(resp => {
        console.log(resp);
        this.userEmail = resp.user.email; console.log(this.userEmail);

        if (this.userEmail === 'hrobachok1ua@gmail.com') {
          this.admin = true;
        }

        this.r.navigate(['/']);
      }).catch(e => console.log(e.message));
  }

  signUpUser(email, password) {
    this.userEmail = '';
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(resp => {
        firebase.auth().currentUser.sendEmailVerification()
          .then(() => {
            this.checkEmail = true;
            console.log('check email');
          });
        this.userEmail = resp.user.email; console.log(this.userEmail);

        if (this.userEmail === 'hrobachok1ua@gmail.com') {
          this.admin = true;
        }

        this.r.navigate(['/']);
      })
      .catch(e => {
        this.alreadyReg = true;
      });
  }

  getPosts() {
    this.postRef = this.http.list('/posts');
    return this.posts = this.postRef.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      })
    );
  }
  getPostsNoSend() {
    this.postRef = this.http.list('/posts');
    return this.posts = this.postRef.snapshotChanges().pipe(
      map(changes => {
        return changes.filter(el => !el.payload.val().post)
          .map(c => ({
            key: c.payload.key, ...c.payload.val()
          }));
      })
    );
  }
  getPostsOnlySend() {
    this.postRef = this.http.list('/posts');
    return this.posts = this.postRef.snapshotChanges().pipe(
      map(changes => {
        return changes.filter(el => el.payload.val().post)
          .map(c => ({
            key: c.payload.key, ...c.payload.val()
          }));
      })
    );
  }
  sendPost(v) {
    return this.http.list('/posts')
      .push(v);
  }
  // deletePost(i) {
  //   return this.http.list('/posts').remove(i.key);
  // }
}
