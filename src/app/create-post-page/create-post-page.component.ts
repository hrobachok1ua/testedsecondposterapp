import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostsAndUsersService } from '../posts-and-users.service';
import { Router } from '@angular/router';
import { ExitGuard } from '../exit.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-post-page',
  templateUrl: './create-post-page.component.html',
  styleUrls: ['./create-post-page.component.scss']
})
export class CreatePostPageComponent implements OnInit, OnDestroy {

  swModal = false;
  letGuardPass = false;

  posts;

  type = 'News';
  post = true;
  author = '';
  keyCode = Math.floor(Math.random() * 999999);

  description = '';

  constructor(private postsAndUsersService: PostsAndUsersService, private r: Router) { }

  ngOnInit() {
    if (this.postsAndUsersService.userEmail === '') {
      this.author = 'Anonim';
    } else {
      this.author = this.postsAndUsersService.userEmail;
    }

    this.postsAndUsersService.getPostsNoSend()
      .subscribe(post => {
        this.posts = post.sort((a, b) => {
          if (a.date > b.date) {
            return 1;
          }
          if (a.date < b.date) {
            return -1;
          }
          return 0;
        });
      });
  }

  showModal() {
    this.swModal = true;
  }
  hideModal() {
    this.swModal = false;
  }

  createPost(v) {
    this.letGuardPass = true;
    this.postsAndUsersService.sendPost(v);
    this.r.navigate(['/']);
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (this.description !== '' && !this.letGuardPass) {
      return confirm('Your data in description won\'t save');
    } else {
      return true;
    }
  }
  ngOnDestroy() {
    this.letGuardPass = false;
  }

}
