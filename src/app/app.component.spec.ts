import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { PostsAndUsersService } from './posts-and-users.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'postApp'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('postApp');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to postApp!');
  });
  it('should contain userEmail', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    let compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelectorAll('button')[1].textContent).toContain($PostsAndUsersService.userEmail);
  })
  it('should display name when logged in', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    let compiled = fixture.debugElement.nativeElement;

    fixture.detectChanges();

    if ($PostsAndUsersService.userEmail !== '') {
      expect(compiled.querySelectorAll('button')[1].textContent).toContain($PostsAndUsersService.userEmail);
    }
  })

});
