import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { CreatePostPageComponent } from './create-post-page/create-post-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { PostPageComponent } from './home-page/post-page/post-page.component';

import { ExitGuard } from './exit.guard';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'register', component: RegisterPageComponent },
  { path: 'create', component: CreatePostPageComponent, canDeactivate: [ExitGuard] },
  { path: 'user-account', component: UserPageComponent },
  { path: 'details/:id', component: PostPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
