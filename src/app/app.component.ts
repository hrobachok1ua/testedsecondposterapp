import { Component, DoCheck } from '@angular/core';
import { PostsAndUsersService } from './posts-and-users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements DoCheck {

  text = 'Sign up/in';
  logged = false;
  showModal = false;

  userEmail = this.service.userEmail;
  admin = this.service.admin;
  checkEmail = this.service.checkEmail;
  changed = this.service.changed;
  alreadyReg = this.service.alreadyReg;
  deletedAcc = this.service.deletedAcc

  constructor(public service: PostsAndUsersService, private r: Router) { }

  ngDoCheck() {
    if (this.service.userEmail !== '') {
      this.logged = true;
    } else {
      this.logged = false;
    }
  }

  hideChanged() {
    this.service.changed = false;
  }

  hideCheckEmail() {
    this.service.checkEmail = false;
    this.service.deletedAcc = false;
  }

  hideModal() {
    this.showModal = false;
  }

  hideAlreadyReg() {
    this.service.alreadyReg = false;
  }

  logOut() {
    this.showModal = true;
  }
  logOut2() {
    this.showModal = false;
    this.service.userEmail = '';
    this.service.admin = false;
    this.r.navigate(['/']);
  }
}
